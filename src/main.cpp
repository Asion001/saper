#include <iostream>
#include "saper.hpp"

int main(int argc, char* args[])
{
    using namespace std;
    int mode = 1;

    if (argc > 1)
    {
        string x; 
        x = args[argc - 1];
        if (x == "0") mode = 0;
        else if (x == "1") mode = 1;
    }
    Saper saper(8,8);
    cout << saper.get_str_board(mode) << endl;
    return 0;
}
