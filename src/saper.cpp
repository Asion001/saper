#include "saper.hpp"
#include <string>
#include <iostream>
#include <random>
#include <time.h>

using namespace std;


void Saper::gen_board()
{
    gen_mine();
    #ifdef DEBUG
    cout << get_str_board() << endl;
    #endif
    gen_numbers();
}


string Saper::get_str_board(short mode)
{
    string tmp;

    std::string discord_text[] = {
    ":zero:", ":one:", ":two:", ":three:",
    ":four:", ":five:", ":six:",
    ":seven:",":eight:",":bomb:"};
    
    for (auto row = 0; row < size_board_x; row++)
    {
        for (auto i = 0; i < size_board_y; i++)
        {
            if (mode == 0)
            {
                tmp += "||";
                if (board[row][i] == 9) tmp += discord_text[9];
                else tmp += discord_text[board[row][i]];
                tmp += "||";
            }
            if (mode == 1)
            {
                if (board[row][i] == 9) tmp += "X";
                else tmp += to_string(board[row][i]);
                tmp += " ";
            } 
        }
        tmp += "\n";
    }
    return tmp;
}


void Saper::gen_mine()
{
    srand (time(NULL));
    for (auto row = 0; row < size_board_x; row++)
    {
        for (auto i = 0; i < size_board_y; i++)
        {
            if (rand() % 10 > board_dificult) board[row][i] = 0;
            else board[row][i] = 9;
        }
    }    
}


void Saper::gen_numbers()
{
    for (int row = 0; row < size_board_x; row++)
    {
        for (int i = 0; i < size_board_y; i++)
        {
            if (board[row][i] != 9)
            {
                int st_x, st_y, endx, endy;

                if (row <= 0) st_x = 0;
                else st_x = row - 1;
                if (i <= 0) st_y = 0;
                else st_y = i - 1;

                if (row < size_board_x) endx = row + 1;
                else endx = size_board_x;
                if (i < size_board_y) endy = i + 1 ;
                else endy = size_board_y;

            }
        }
    }    
}


void Saper::set_size_board(int size_x, int size_y) 
{
    size_board_x = size_x;
    size_board_y = size_y;
}


auto Saper::get_size(bool x) 
{
    if (x == true) return size_board_x;
    else return size_board_y;             
}


auto Saper::get_board() 
{
    return board;
}


void Saper::set_dificult(int x)
{
    board_dificult = x;
}
